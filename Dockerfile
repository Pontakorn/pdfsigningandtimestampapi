FROM python:3-alpine

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/

#RUN apk add --no-cache mariadb-dev build-base
RUN apk add --no-cache mariadb-connector-c-dev ;\
    apk add --no-cache --virtual .build-deps \
        build-base \
        mariadb-dev ;\
    pip install mysqlclient;\
    apk del .build-deps 
RUN apk add bash
RUN apk add tzdata
ENV TZ=Asia/Bangkok
RUN pip3 install --no-cache-dir -r requirements.txt

COPY . /usr/src/app

EXPOSE 8080

# ENTRYPOINT [""]

# CMD ["python3", "-m", "swagger_server"]
CMD ["python3", "tempTest.py"]