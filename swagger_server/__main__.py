#!/usr/bin/env python3
import connexion
from swagger_server.trigger import updateInformation
from threading import Timer
from datetime import datetime, timedelta
import threading
import time
import logging
import logging.handlers
# logger = logging.getLogger(__name__)
# handler = logging.StreamHandler()
# formatter = logging.Formatter('%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s')
# handler.setFormatter(formatter)
# logger.addHandler(handler)
# logger.setLevel(logging.DEBUG)

logging.basicConfig(filename='logTest.log',
                            filemode='a',
                            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                            datefmt='%H:%M:%S',
                            level=logging.DEBUG)
console = logging.StreamHandler()
console.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)

# my_logger = logging.getLogger('MyLogger')
# my_logger.setLevel(logging.DEBUG)
# handler = logging.handlers.SysLogHandler(address = '/dev/log')
# my_logger.addHandler(handler)


def main():
    app = connexion.App(__name__, specification_dir='./swagger/')
    app.add_api('swagger.yaml', arguments={'title': 'Swagger Petstore'})
    app.run(port=8080)

def updateEveryDay():
    today = datetime.today()
    tomorrow = today.replace(day=today.day, hour=5) + timedelta(days=1)
    delta_t = tomorrow - today
    secs = delta_t.total_seconds()
    trigger = Timer(secs, updateEveryDay)
    trigger.start()
    time.sleep(10.0)
    updateInformation.GetAndInsertStudentCourse()
    

if __name__ == '__main__':
    logging.info("start main")
    mainThreading = threading.Thread(target=main)
    mainThreading.start()
    updateEveryDay()