import connexion
import six
from swagger_server.models.api_response import ApiResponse  # noqa: E501
from swagger_server import util
from swagger_server.models.information import Information

informationModel = Information()


def GetCourseById(courseId):
    if len(courseId) == 0:
        return "Please provide course Id.", 400
    return informationModel.GetCourse(courseId)


def GetCourses():
    temp = informationModel.GetCourses()
    if type(temp) is bool:
        return "Cannot get all course from database", 400
    return temp
