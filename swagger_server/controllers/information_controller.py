import connexion
import six
from swagger_server.models.api_response import ApiResponse  # noqa: E501
from swagger_server import util
from swagger_server.models.information import Information
import json
from flask import render_template
import base64

informationModel = Information()


def home():
    tempAllCourses = informationModel.GetCourses()
    return render_template('home.html', detail=tempAllCourses)


def information(q):  # noqa: E501
    dParam = base64.b64decode(q)
    sParam = dParam.decode('ascii')
    arrParam = sParam.split("/")
    studentCode = arrParam[0]
    courseId = arrParam[1]
    returnObj = {
        "student": "",
        "course": "",
        "description": "",
        "success": False
    }
    try:
        studentCode = int(studentCode)
        studentModel = informationModel.GetStudent(studentCode)
        courseModel = informationModel.GetCourse(courseId)
    except Exception as e:
        returnObj["description"] = "Student code cannot be alphabet or include alphabet or empty value."
        return render_template('detail.html', title='Information Student', detail=returnObj)
    if studentCode <= 0 or len(courseId) == 0:
        returnObj["description"] = "Student code will be more than 0 or Course ID cannot empty value."
        return render_template('detail.html', title='Information Student', detail=returnObj)
    if informationModel.CheckStudentInCourse(studentCode, courseId).count() == 0:
        returnObj["description"] = "Student: {} have not registerd on course: {}".format(studentCode, courseModel["title"])
        return render_template('detail.html', title='Information Student', detail=returnObj)

    returnObj["student"] = studentModel
    returnObj["course"] = courseModel
    returnObj["success"] = True

    return render_template('detail.html', title='Information Student', detail=returnObj)


def GeneratePDFFlag(isPrint, studentCode, courseId):
    informationModel.setIsPrint(isPrint, studentCode, courseId)


def GenerateCer(isType):
    temp = informationModel.GetAllData(isType)
    return temp, 200
