import connexion
import six
from swagger_server.models.api_response import ApiResponse  # noqa: E501
from swagger_server import util
from swagger_server.models.information import Information

informationModel = Information()


def GetStudent(studentCode):
    if studentCode == None or studentCode <= 0:
        return "Please provide student code.", 400
    return informationModel.GetStudent(studentCode)
