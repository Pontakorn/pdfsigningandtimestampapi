import mysql.connector


def create_database():
    mydb = mysql.connector.connect(host='10.110.38.11', user='cert', passwd='Le-Mooc', port=3306)
    # mydb = mysql.connector.connect(host='localhost',user='root',passwd='Abc123admin')
    mycor = mydb.cursor()
    mycor.execute("CREATE DATABASE IF NOT EXISTS lifelong_cert character set utf8 collate utf8_general_ci;")
    mycor.execute("use lifelong_cert;")
    mycor.execute("create table IF NOT EXISTS tbcourse( id varchar(255) primary key, \
        code varchar(255) not null, \
        title varchar(255) not null, \
        principle varchar(255) not null, \
        objective varchar(255) not null, \
        detail text, \
        facultyName varchar(255) not null \
        );")
    mycor.execute("create table IF NOT EXISTS tbstudent( code int primary key not null, \
        nameTh varchar(255) not null, \
        nameEn varchar(255) not null, \
        email varchar(255), \
        phone varchar(16) \
        );")
    mycor.execute("create table IF NOT EXISTS tbstudentcourse( studentCourseId int auto_increment primary key, \
        studentCode int not null, \
        courseId varchar(255) not null, \
        isActive boolean default true not null, \
        isPrint boolean default true not null, \
        createdDate timestamp default current_timestamp, \
        FOREIGN KEY (studentCode) REFERENCES tbstudent(code), \
        FOREIGN KEY (courseId) REFERENCES tbcourse(id) \
        );")
    mycor.execute("create table IF NOT EXISTS tbconfig( id int auto_increment primary key, \
        name varchar(255) not null, \
        value varchar(255) not null \
        );")
    mydb.commit()
    mycor.close()
    mydb.close()
