import base64


def do_encode(value):
    if value is None:
        return ''
    valueByte = value.encode('ascii')
    return base64.b64encode(valueByte)
