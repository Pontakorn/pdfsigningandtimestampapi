from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from swagger_server.db import createDB
from sqlalchemy import exc
from sqlalchemy import event
from sqlalchemy.pool import Pool

# createDB.create_database()
app_flask = Flask(__name__)
app_flask.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://cert:Le-Mooc@10.110.38.11:3306/lifelong_cert?charset=utf8'
# Replacing localhost by host.docker.internal for a test in Docker
# app_flask.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:Pontakorn2@localhost/lifelong_cert?charset=utf8'
# app_flask.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:Abc123admin@host.docker.internal/information?charset=utf8'
# app_flask.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:Abc123admin@localhost/information?charset=utf8'
db = SQLAlchemy(app_flask)


class Tbstudent(db.Model):
    code = db.Column(db.Integer, primary_key=True)
    nameTh = db.Column(db.String(120), nullable=False)
    nameEn = db.Column(db.String(120), nullable=False)
    phone = db.Column(db.Integer)
    email = db.Column(db.String(255), nullable=False)
    studentCourses = db.relationship('Tbstudentcourse', backref='tbstudent', lazy=True)

    def __repr__(self):
        return '<Tbstudent {}>'.format(self.code)


class Tbcourse(db.Model):
    id = db.Column(db.String(255), primary_key=True)
    code = db.Column(db.String(255), nullable=False)
    objective = db.Column(db.String(255), nullable=False)
    detail = db.Column(db.String(255))
    principle = db.Column(db.String(255), nullable=False)
    title = db.Column(db.String(255), nullable=False)
    facultyName = db.Column(db.String(255), nullable=False)
    lesson = db.Column(db.String(255))
    data = db.Column(db.String(255))
    studentCourses = db.relationship('Tbstudentcourse', backref='tbcourse', lazy=True)

    def __repr__(self):
        return '<Tbcourse {}>'.format(self.code)


class Tbstudentcourse(db.Model):
    studentCourseId = db.Column(db.Integer, primary_key=True)
    studentCode = db.Column(db.Integer, db.ForeignKey('tbstudent.code'), nullable=False)
    courseId = db.Column(db.String(255), db.ForeignKey('tbcourse.id'), nullable=False)
    createdDate = db.Column(db.DateTime, nullable=False)
    isActive = db.Column(db.Boolean)
    isPrint = db.Column(db.Boolean)
    isType = db.Column(db.Integer)

    def __repr__(self):
        return '<Tbstudentcourse {}>'.format(self.studentCourseId)


class Tbconfig(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    value = db.Column(db.String(255), nullable=False)

    def __repr__(self):
        return '<Tbstudentcourse {}>'.format(self.id)


db.create_all()
db.session.commit()
