# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401
from typing import List, Dict  # noqa: F401
from swagger_server.models.base_model_ import Model
from swagger_server import util
from swagger_server.models.db import *


class Information(Model):
    def __init__(self):
        pass

    def CheckStudentInCourse(self, student, course):
        temp = []
        try:
            temp = Tbstudentcourse.query.filter(
                (Tbstudentcourse.studentCode == student) & (Tbstudentcourse.courseId == course) & (
                        Tbstudentcourse.isActive == True))
            db.session.commit()
            return temp
        except:
            db.session.rollback()

    def CheckDuplicateCourse(self, courseId):
        temp = []
        try:
            temp = Tbcourse.query.filter_by(id=courseId).count()
            db.session.commit()
            return temp
        except:
            db.session.rollback()

    def CheckDuplicateStudent(self, studentCode):
        temp = []
        try:
            temp = Tbstudent.query.filter_by(code=studentCode).count()
            db.session.commit()
            return temp
        except:
            db.session.rollback()

    def GetStudent(self, studentCode):
        self._student = dict()
        try:
            temp = Tbstudent.query.filter_by(code=studentCode).first()
            db.session.commit()
            if temp != None:
                self._student["code"] = temp.code
                self._student["nameTh"] = temp.nameTh
                self._student["nameEn"] = temp.nameEn
                self._student["phone"] = temp.phone if not temp.phone == None else '-'
                self._student["email"] = temp.email
        except:
            db.session.rollback()

        return self._student

    def GetCourse(self, courseCode):
        self._course = dict()
        try:
            temp = Tbcourse.query.filter_by(id=courseCode).first()
            db.session.commit()
            if temp != None:
                # print(temp.detail, type(temp.detail))
                self._course["id"] = temp.id
                self._course["objective"] = temp.objective
                self._course["principle"] = temp.principle
                self._course["detail"] = temp.detail if not temp.detail == None and len(temp.detail) > 0 else '-'
                self._course["title"] = temp.title
                self._course["code"] = temp.code
                self._course["facultyName"] = temp.facultyName if not temp.facultyName == None and len(
                    temp.facultyName) > 0 else '-'
        except:
            db.session.rollback()

        return self._course

    def GetCourses(self):
        temp = []
        try:
            temp = Tbcourse.query.all()
            db.session.commit()
            if temp != None:
                courses = []
                for c in temp:
                    course = {
                        "id": c.id,
                        "objective": c.objective,
                        "printciple": c.principle,
                        "detail": c.detail if not c.detail == None and len(c.detail) > 0 else '-',
                        "title": c.title,
                        "code": c.code,
                        "facultyName": c.facultyName if not c.facultyName == None and len(c.facultyName) > 0 else '-'
                    }
                    courses.append(course)
                return courses
        except:
            db.session.rollback()
        return False

    def setIsPrint(self, isPrint, studentCode, courseId):
        temp = []
        try:
            temp = Tbstudentcourse.query.filter(
                (Tbstudentcourse.studentCode == studentCode) & (Tbstudentcourse.courseId == courseId)).first()
            temp.isPrint = isPrint
            db.session.commit()
        except:
            db.session.rollback()

    def GetConfig(self, name):
        temp = []
        try:
            temp = Tbconfig.query.filter_by(name=name).first()
            db.session.commit()
            if temp != None:
                return temp.value
        except:
            db.session.rollback()

    def GetAllData(self, isType):
        tempSC = []
        try:
            tempSC = Tbstudentcourse.query.filter(
                (Tbstudentcourse.isType == isType) & (Tbstudentcourse.isPrint == False) & (
                        Tbstudentcourse.isActive == True)).order_by(Tbstudentcourse.courseId).all()
        except:
            db.session.rollback()

        tempDetails = []
        tempCName = ''
        objDetail = dict()
        for sc in tempSC:
            if sc == None:
                continue
            try:
                tempCourse = Tbcourse.query.filter_by(id=sc.courseId).first()
                if tempCourse == None:
                    raise
                if tempCName == '' or tempCName != sc.courseId:
                    if tempCName != '':
                        tempDetails.append(objDetail)
                    tempCName = sc.courseId
                    objDetail = {
                        'course_id': sc.courseId,
                        'course_code': tempCourse.code,
                        'course_name': tempCourse.title,
                        'course_organization': tempCourse.facultyName,
                        'course_detail': tempCourse.detail,
                        'course_lesson': tempCourse.lesson,
                        'course_data': tempCourse.data,
                        'students': []
                    }
                tempStudent = Tbstudent.query.filter_by(code=sc.studentCode).first()
                if tempStudent == None:
                    raise
                tempStudentDetail = {
                    'learner_id': tempStudent.code,
                    'learner_name': tempStudent.nameTh,
                    'learner_name_eng': tempStudent.nameEn,
                    'email': tempStudent.email.split(":")[1]
                }
                objDetail['students'].append(tempStudentDetail)
            except Exception as e:
                print(str(e))
                db.session.rollback()
                continue
        tempDetails.append(objDetail)
        db.session.commit()
        return tempDetails
