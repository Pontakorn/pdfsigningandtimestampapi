import requests
from swagger_server.models.information import *
from swagger_server.models.db import *
import json
from datetime import datetime
import re
import logging
import time

# logger = logging.getLogger(__name__)
# handler = logging.StreamHandler()
# formatter = logging.Formatter('%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s')
# handler.setFormatter(formatter)
# logger.addHandler(handler)
# logger.setLevel(logging.DEBUG)

logging.basicConfig(filename='logTest.log',
                            filemode='a',
                            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                            datefmt='%H:%M:%S',
                            level=logging.DEBUG)

# define a Handler which writes INFO messages or higher to the sys.stderr
console = logging.StreamHandler()
console.setLevel(logging.INFO)
# set a format which is simpler for console use
formatter = logging.Formatter('%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s')
# tell the handler to use this format
console.setFormatter(formatter)
# add the handler to the root logger
logging.getLogger('').addHandler(console)
time.sleep(20.0)
for i in range(10):
    logging.info(i)


informationModel = Information()
headersStudentInCourse = {
    "Authorization": informationModel.GetConfig('authCourse')
}
headerStudent = {
    "Authorization": informationModel.GetConfig('authStudent')
}
studentParam = {
    "key": informationModel.GetConfig('keyStudent'),
    "service": informationModel.GetConfig('serviceStudent'),
    "email": ""
}
apiMooc = informationModel.GetConfig('apiMooc')
moocScore = int(informationModel.GetConfig('moocScore'))
resCFreeJson = {
    'data': []
}
resCPayJson = {
    'data': []
}


def GetAndInsertStudentCourse():
    responseCoursePay = requests.get('{}coursetype/{}'.format(apiMooc, informationModel.GetConfig('typepay')),
                                     headers=headersStudentInCourse, timeout=120)
    responseCourseFree = requests.get('{}coursetype/{}'.format(apiMooc, informationModel.GetConfig('typefree')),
                                      headers=headersStudentInCourse, timeout=120)

    if responseCourseFree.status_code >= 400:
        logging.error("Cannot get courses on free type.")
        resCFreeJson['data'] = []
    if responseCoursePay.status_code >= 400:
        logging.error("Cannot get course on pay type.")
        resCPayJson['data'] = []

    resCFreeJson = json.loads(responseCourseFree.text)
    resCPayJson = json.loads(responseCoursePay.text)
    if resCFreeJson is None or resCFreeJson['data'] is None or \
    len(resCFreeJson['data']) == 0 or not resCFreeJson['status']:
        logging.error("Cannot get Course Free")
        resCFreeJson['data'] = []

    if resCPayJson is None or resCPayJson['data'] is None or \
        len(resCPayJson['data']) == 0 or not resCPayJson['status']:
        logging.error("Cannot get Course Pay")
        resCPayJson['data'] = []

    for cPay in resCPayJson['data']:
        if cPay['IsCourseActive']:
            try:
                InsertAllDataToDb(cPay['CourseID'], 2)
            except Exception as e:
                logging.error(e)
                continue
    logging.info("Get MOOC Pay Done.")

    for cFree in resCFreeJson['data']:
        if cFree['CourseID'] is None or not cFree['IsCourseActive']:
            continue
        try:
            logging.info(cFree['CourseID'])
            InsertAllDataToDb(cFree['CourseID'], 1)
        except Exception as e:
            logging.error(e)
            continue
    logging.info("Get MOOC Free Done.")


def InsertAllDataToDb(courseId, c_type):
    responseReport = requests.get('{}coursereports/{}'.format(apiMooc, courseId), headers=headersStudentInCourse,
                                  timeout=120)
    responseCourse = requests.get('{}coursemodule/{}'.format(apiMooc, courseId), headers=headersStudentInCourse,
                                  timeout=120)
    if responseReport.status_code >= 400 or responseCourse.status_code >= 400:
        logging.error("Cannot get course report")
    else:
        if len(responseReport.text) == 0 or len(responseCourse.text) == 0:
            logging.error("Response is empty on both course report and detail.")
        else:
            resJson = json.loads(responseReport.text)
            resCourseJson = json.loads(responseCourse.text)
            if resCourseJson["course_active"] == False:
                logging.error("course {} cannot insert because active is {}".format(resCourseJson["course_id"],
                                                                            resCourseJson["course_active"]))
                return
            if resJson['course_id'] is None or resCourseJson['course_id'] is None or resCourseJson['course_code'] is None or \
                resCourseJson['course_name'] is None:
                logging.error("Course id or Course code or course name cannot be null")
                return
            if resJson['course_id'] != resCourseJson['course_id']:
                logging.error("Course id on report and module is not matched")
                return
            if len(resJson['course_id']) == 0 or len(resCourseJson["course_code"]) == 0 or len(
                    resCourseJson["course_name"]) == 0:
                logging.error("Course ID or Course code or Course name are empty")
            else:
                courseModel = Tbcourse()
                courseModel.id = resJson['course_id']
                if len(resCourseJson['data']) == 0:
                    courseModel = None
                    logging.error("Cannot get detail of course: {}".format(courseModel.id))
                    return
                if resJson['data'] is None or resCourseJson['data'] is None or len(resJson['data']) == 0 or len(
                        resCourseJson['data']) == 0:
                    courseModel = None
                    logging.error("Cannot get data of student in course: {}".format(courseModel.id))
                    return
                else:
                    if informationModel.CheckDuplicateCourse(courseModel.id) == 0:
                        courseModel.code = resCourseJson['course_code']
                        courseModel.title = resCourseJson['course_name']
                        courseModel.objective = 'blank'
                        courseModel.principle = 'blank'
                        courseModel.facultyName = resCourseJson['course_organization']
                        courseModel.data = ','.join([dt['name'] for dt in resCourseJson['data']])

                        if len(resCourseJson['course_detail']) == 0:
                            courseModel.detail = resCourseJson['course_detail']
                        else:
                            cleanr = re.compile('<.*?>')
                            cleantext = re.sub(cleanr, '\n', resCourseJson['course_detail'])
                            temp = cleantext.split('\n')
                            courseModel.detail = '\n'.join(x.strip() for x in temp if len(x.strip()) > 0).replace(
                                '&nbsp;', '')

                        db.session.add(courseModel)
                    for data in resJson['data']:
                        if data['certificate_status'] is None or data['course_score'] is None or '@' not in data[
                            'Canvas_LoginID'] or \
                                data['certificate_status'] == "Fail" or len(data['Canvas_LoginID']) == 0 or data[
                            'course_score'] < moocScore or \
                                '@' not in data['Canvas_LoginID']:
                            continue
                        if data['certificate_status'] == "Pass" and data['course_score'] >= moocScore:
                            studentModel = Tbstudent()
                            studentModel.email = data['Canvas_LoginID']
                            studentParam['email'] = studentModel.email
                            responseStudent = requests.post(informationModel.GetConfig('urlLeaner'),
                                                            data=json.dumps(studentParam), headers=headerStudent,
                                                            timeout=120)
                            if responseStudent.status_code >= 400 or len(responseStudent.text) == 0:
                                continue
                            tempStudentModel = json.loads(responseStudent.text)
                            if tempStudentModel is None or tempStudentModel['status'] is None or tempStudentModel['message'] is None or \
                                tempStudentModel['status'] == 0:
                                logging.error("Response of the student is None or Status and Detail of student is None or status of student not 1.")
                                continue
                            if len(tempStudentModel['message']['learner_id']) == 0 or len(
                                    tempStudentModel['message']['learner_name']) == 0 \
                                    or len(tempStudentModel['message']['learner_name_eng']) == 0:
                                logging.error("Cannot insert student {} because Id or Name or Name Eng are empty.".format(
                                    studentModel.email))
                                studentModel = None
                                continue
                            try:
                                studentModel.code = int(tempStudentModel["message"]["learner_id"])
                            except Exception as e:
                                logging.error('Student code cannot be alphabet or include alphabet')
                                studentModel = None
                                continue
                            if informationModel.CheckStudentInCourse(studentModel.code, courseModel.id).count() == 0:
                                if len(tempStudentModel['message']['learner_name_eng']) == 0 or len(
                                        tempStudentModel['message']['learner_name']) == 0:
                                    logging.error("Cannot insert student code {} because nameTh or nameEng is empty".format(
                                        studentModel.code))
                                    continue
                                if informationModel.CheckDuplicateStudent(studentModel.code) == 0:
                                    studentModel.nameEn = tempStudentModel['message']['learner_name_eng']
                                    studentModel.nameTh = tempStudentModel['message']['learner_name']
                                    studentModel.phone = tempStudentModel['message']['learner_phone']
                                    studentModel.email = studentModel.email + ":" + tempStudentModel['message'][
                                        'learner_email']
                                    db.session.add(studentModel)
                                scModel = Tbstudentcourse()
                                scModel.courseId = courseModel.id
                                scModel.studentCode = studentModel.code
                                scModel.isActive = 1
                                scModel.isPrint = 0
                                scModel.isType = c_type
                                scModel.createdDate = datetime.strptime(datetime.now().strftime('%m-%d-%Y %H:%M:%S'),
                                                                        '%m-%d-%Y %H:%M:%S')
                                db.session.add(scModel)
                    db.session.commit()
