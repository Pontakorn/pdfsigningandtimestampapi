import logging
import time

logging.basicConfig(filename="error.log", level=logging.DEBUG)

while True:
  start_time = time.time()
  logging.debug("Program starts running at %d", start_time)
  time.sleep(2)
  logging.debug("time time %d", time.time())
